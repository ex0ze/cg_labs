#include "mainwindow.h"
#include <QDebug>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "algo.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->surfaceLayout->addWidget(QWidget::createWindowContainer(&surface));
}
void MainWindow::on_rotateX_pressed() {
    for (auto i : *data)
        for (auto &j : *i)
            j.setX(-j.x());

    update_data();
    surface.axisX()->setRange(-surface.axisX()->max(), -surface.axisX()->min());
}

void MainWindow::on_rotateY_pressed() {
    for (auto i : *data)
        for (auto &j : *i)
            j.setY(-j.y());

    update_data();
}

void MainWindow::update_data() {
    QSurface3DSeries *series = new QSurface3DSeries;
    series->dataProxy()->resetArray(data);
    for (auto &it : surface.seriesList())
        surface.removeSeries(it);
    surface.addSeries(series);
    surface.show();
}

MainWindow::~MainWindow() {
    delete ui;
}
#include <QMessageBox>
void MainWindow::on_buildSurfaceBtn_clicked()
{
    std::array<QVector3D, 4> coords_input;
    bool ok_x = true, ok_y = true, ok_z = true;
    for (int i = 0; i < 4; ++i)
    {
        coords_input[i].setX(ui->coordTable->item(i, 0)->text().toDouble(&ok_x));
        coords_input[i].setY(ui->coordTable->item(i, 1)->text().toDouble(&ok_y));
        coords_input[i].setZ(ui->coordTable->item(i, 2)->text().toDouble(&ok_z));
        if (!ok_x || !ok_y || !ok_z)
        {
            QMessageBox::critical(this, "Ошибка ввода", QString("Ошибка ввода в строке %1 в координате %2").arg(i + 1).arg(!ok_x ? "X" : !ok_y ? "Y" : "Z"));
            return;
        }
    }
    QSurface3DSeries *series = new QSurface3DSeries;
    data = getData(coords_input, .01f);
    series->dataProxy()->resetArray(data);
    surface.addSeries(series);
    surface.show();
    ui->rotateX->setEnabled(true);
    ui->rotateY->setEnabled(true);
}
