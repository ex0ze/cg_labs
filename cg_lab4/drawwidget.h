#ifndef DRAWWIDGET_H
#define DRAWWIDGET_H

#include <QObject>
#include <QWidget>
#include <QLine>
#include <random>
#include <QPainter>
#include <QList>

class DrawWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DrawWidget(QWidget *parent = nullptr);
public slots:
    void generateLines(int count);
    void setRect(int x_min, int y_min, int x_max, int y_max);
    void paintEvent(QPaintEvent *event) override;
signals:
private:
    QList<QLine> m_lines;
    QRect m_rect;

    void super_algorithm_NHK2000ZXC();
    double scalar(QPoint a, QPoint b);
    QPoint normal(QPoint a, QPoint b, QPoint z);
    void CyrusBeckAlgorithm(QVector<QPoint> polygon, QPoint a, QPoint b, QPainter &painter);

#endif // DRAWWIDGET_H
};
