#include "drawwidget.h"

DrawWidget::DrawWidget(QWidget *parent) : QWidget(parent)
{

}

void DrawWidget::generateLines(int count)
{
    m_lines.clear();
    int x_min, x_max, y_min, y_max;
    x_min = this->rect().topLeft().x();
    y_min = this->rect().topLeft().y();

    x_max = this->rect().bottomRight().x();
    y_max = this->rect().bottomRight().y();
    std::mt19937 generator(time(0));
    std::uniform_int_distribution<int> distribution_x(x_min,x_max);
    std::uniform_int_distribution<int> distribution_y(y_min,y_max);
    for (int i = 0; i < count ; ++i)
    {
        int x_start = distribution_x(generator);
        int y_start = distribution_y(generator);
        int x_end = distribution_x(generator);
        int y_end = distribution_y(generator);
        m_lines << QLine(x_start, y_start, x_end, y_end);
    }
    QWidget::repaint();
}

void DrawWidget::setRect(int x_min, int y_min, int x_max, int y_max)
{
    m_rect.setTopLeft({x_min, y_min});
    m_rect.setBottomRight({x_max, y_max});
    QWidget::repaint();
}

void DrawWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    painter.setPen(QPen(Qt::darkMagenta, 2, Qt::SolidLine, Qt::FlatCap));
    painter.drawRect(m_rect);
    painter.setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::FlatCap));
    for (const auto & line : m_lines) painter.drawLine(line);
    for (const auto & line : m_lines)
        CyrusBeckAlgorithm({m_rect.topLeft(), m_rect.topRight(), m_rect.bottomRight(), m_rect.bottomLeft(), m_rect.topLeft()}, line.p1(), line.p2(), painter);
}

void DrawWidget::super_algorithm_NHK2000ZXC()
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::red, 3, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    for (const auto & line : m_lines)
    {

    }
}

double DrawWidget::scalar(QPoint a, QPoint b)
{
    return double(a.x() * b.x() + a.y() * b.y());
}

QPoint DrawWidget::normal(QPoint a, QPoint b, QPoint z)
{
    QPoint d = b - a;
    QPoint n(-d.y(), d.x());
    QPoint v(z.x() - a.x(), z.y() - a.y());

    double dot = scalar(v, n);

    if (fabs(dot) <= std::numeric_limits<double>::epsilon()) {
        return QPoint();
    } else if (dot < 0) {
        n *= -1;
    }

    return n;
}

void DrawWidget::CyrusBeckAlgorithm(QVector<QPoint> polygon, QPoint a, QPoint b, QPainter &painter)
{
    QPoint d = b - a;
    QPoint dir(d.x(), d.y());

    double tEnter = 0.0;
    double tLeave = 1.1;

    QPoint boundary(polygon[polygon.size() - 2]);

    for (size_t i = 0; i < polygon.size() - 1; ++i) {
        QPoint edge_p0(polygon[i]);
        QPoint edge_p1(polygon[i + 1]);

        QPoint norm(normal(edge_p0, edge_p1, boundary));
        QPoint w(a.x() - edge_p0.x(), a.y() - edge_p0.y());

        double num = scalar(w, norm);
        double den = scalar(dir, norm);

        if (fabs(den) <= std::numeric_limits<double>::epsilon()) {
            if (num < 0)
                return;
            else
                continue;
        }

        double t = -num / den;

        if (den > 0) {
            tEnter = std::max(tEnter, t);
        } else if (den < 0) {
            tLeave = std::min(tLeave, t);
        }

        boundary = edge_p0;
    }

    if (tEnter > tLeave) {
        return;
    }

    QPoint clipped_p0;
    clipped_p0.setX(a.x() + (d.x() * tEnter));
    clipped_p0.setY(a.y() + (d.y() * tEnter));

    QPoint clipped_p1;
    clipped_p1.setX(a.x() + (d.x() * tLeave));
    clipped_p1.setY(a.y() + (d.y() * tLeave));

    painter.setPen(QPen(Qt::red, 5, Qt::SolidLine, Qt::FlatCap));
//    painter.drawLine(a, clipped_p0);  // начало линии -> начало отсечения
//    painter.setPen(Qt::red);
    painter.drawLine(clipped_p0, clipped_p1);  // отсеченная линия
    painter.setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::FlatCap));
//    painter.setPen(Qt::gray);
//    painter.drawLine(clipped_p1, b);  // конец отсечения -> конец линии
}
