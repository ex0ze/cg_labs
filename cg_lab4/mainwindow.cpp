#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_drawwidget = new DrawWidget(nullptr);
    ui->drawLayout->addWidget(m_drawwidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_gen_btn_clicked()
{
    auto count = ui->line_count_spinbox->value();
    m_drawwidget->generateLines(count);
}

void MainWindow::on_create_rect_btn_clicked()
{
    auto x_min = ui->x_min_spinbox->value();
    auto x_max = ui->x_max_spinbox->value();

    auto y_min = ui->y_min_spinbox->value();
    auto y_max = ui->y_max_spinbox->value();
    m_drawwidget->setRect(x_min, y_min, x_max, y_max);
}
