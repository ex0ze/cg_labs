#include "Menu.h"
#include "Roberts_Algorithm\RAmenu.h"

#include <thread>
#include <iostream>
		
using namespace std;

void menuButton1();
void menuButton2();

int main(void) 
{	
	sfm::Menu mainmenu;
	sfm::MenuOption opt1("Start Roberts_Algorithm", menuButton1);
	mainmenu.addOption(opt1);
	sfm::MenuOption opt2("Exit", menuButton2);
	mainmenu.addOption(opt2);
	mainmenu.setLayout(sfm::MenuLayout::VerticleCentered);
	mainmenu.setBackground("sfml-2.png");
	sf::Font font;
	sf::Text text;
	font.loadFromFile("ARIAL.ttf");
	text.setFont(font);
	text.setFillColor(sf::Color(0, 45, 109));
	mainmenu.setTemplateText(text);
	sf::RenderWindow window;
	window.create(sf::VideoMode(800, 800), "Set of sfmll graphical programms");
	mainmenu.display(window);
	window.close();
	return EXIT_SUCCESS;
}

void menuButton1() {
	std::thread(&RobertsAlgorithm::Roberts_algh_impl).detach();
}

void menuButton2() {
	std::cout << "\nExiting...\n";
    std::exit(EXIT_SUCCESS);
}