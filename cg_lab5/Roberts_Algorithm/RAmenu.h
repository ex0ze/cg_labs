#ifndef RAMENU_H_
#define RAMENU_H_

namespace RobertsAlgorithm
{

int& getWinWidth();
int& getWinHeight();
int Roberts_algh_impl(void);


}


#endif /* RAMENU_H_ */
