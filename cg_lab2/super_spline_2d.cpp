#include "super_spline_2d.h"
#include <QMouseEvent>

super_spline_2d::super_spline_2d(QWidget *parent) : QWidget(parent)
{
}

void super_spline_2d::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(QPen(Qt::red, 5, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    for (auto p : m_points) painter.drawPoint(p);
    if (m_points.count() > 1) drawCasteljau();
}

void super_spline_2d::mousePressEvent(QMouseEvent *event)
{
    this->m_points.push_back(mapFromGlobal(event->globalPos()));
    QWidget::repaint();
}

void super_spline_2d::clear_field()
{
    m_points.clear();
    QWidget::repaint();
}

void super_spline_2d::drawCasteljau()
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::green, 1, Qt::SolidLine, Qt::FlatCap));
    painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    QPoint tmp1, tmp2;
    for (double t = 0.0 ; t < 1.0; t += 0.02)
    {
        tmp1 = getCasteljauPoint(m_points.count() - 1, 0, t);
        if (t > 0.0)
        {
            painter.drawLine(tmp2, tmp1);
        }
        tmp2 = getCasteljauPoint(m_points.count() - 1, 0, t + 0.01);
        painter.drawLine(tmp1, tmp2);
    }
}

QPoint super_spline_2d::getCasteljauPoint(int r, int i, double t)
{
    if (r == 0) return m_points[i];
    QPoint p1 = getCasteljauPoint(r - 1, i, t);
    QPoint p2 = getCasteljauPoint(r - 1, i + 1, t);
    return QPoint((int) ((1 - t) * p1.x() + t * p2.x()), (int) ((1 - t) * p1.y() + t * p2.y()));
}
