#ifndef SUPER_SPLINE_2D_H
#define SUPER_SPLINE_2D_H

#include <QObject>
#include <QPoint>
#include <QVector>
#include <QWidget>
#include <QPixmap>
#include <QPainter>

class super_spline_2d : public QWidget
{
        Q_OBJECT
public:
    super_spline_2d(QWidget* parent);
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
public slots:
    void clear_field();

private:
    QVector<QPoint> m_points;
    QPixmap m_pixmap;
    void drawCasteljau();
    QPoint getCasteljauPoint(int r, int i, double t);
};

#endif // SUPER_SPLINE_2D_H
