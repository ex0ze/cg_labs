#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    auto w = new super_spline_2d(nullptr);
    connect(ui->clearBtn, &QPushButton::clicked, w, &super_spline_2d::clear_field);
    ui->graphLayout->addWidget(w);
}

MainWindow::~MainWindow()
{
    delete ui;
}
