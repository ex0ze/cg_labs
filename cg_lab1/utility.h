#ifndef UTILITY_H
#define UTILITY_H

#include <QVector>
#include <QVector3D>

using Polygon3D = QVector<QVector3D>;
using PolygonList = QList<Polygon3D>;

void rotate_degree(Polygon3D& obj,  double degree, double x, double y, double z);

#endif // UTILITY_H
