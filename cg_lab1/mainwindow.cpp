#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSlider>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    gl_widget = new GLContextWidget(nullptr);
    connect(ui->xSlider, &QSlider::valueChanged, gl_widget, &GLContextWidget::setRot_x);
    connect(ui->ySlider, &QSlider::valueChanged, gl_widget, &GLContextWidget::setRot_y);
    connect(ui->zSlider, &QSlider::valueChanged, gl_widget, &GLContextWidget::setRot_z);

    ui->glWidgetLayout->addWidget(gl_widget);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete gl_widget;
}

