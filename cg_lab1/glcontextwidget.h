#ifndef GLCONTEXTWIDGET_H
#define GLCONTEXTWIDGET_H

#include <QObject>
#include <QWidget>
#include <QTimer>
#include <QVector3D>
#include <QGenericMatrix>
#include <QPolygon>
#include <QGLWidget>
#include <QVector>
#include <QList>
#include <glm/glm.hpp>
#include <GL/glu.h>
#include <unistd.h>
#include "utility.h"

class GLContextWidget : public QGLWidget
{
    Q_OBJECT
public:
    GLContextWidget(QWidget* parent = nullptr);

public slots:

    void setRot_x(float rot_x);

    void setRot_y(float rot_y);

    void setRot_z(float rot_z);

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
private:
    QTimer * m_redraw_timer;
    int m_window_size_x;
    int m_window_size_y;
    float m_rot_x;
    float m_rot_y;
    float m_rot_z;
};

#endif // GLCONTEXTWIDGET_H
