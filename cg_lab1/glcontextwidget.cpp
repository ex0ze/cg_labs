﻿#include "glcontextwidget.h"

GLContextWidget::GLContextWidget(QWidget* parent) : QGLWidget(parent)
{
    m_window_size_x = 500;
    m_window_size_y = 500;
    m_rot_x = 0.0;
    m_rot_y = 0.0;
    m_rot_z = 0.0;
}

void GLContextWidget::initializeGL()
{
    glClearColor(0.6, 0.98, 0.6, 1.0);
    glEnable(GL_DEPTH_TEST);
    m_redraw_timer = new QTimer(this);
    m_redraw_timer->setInterval(10);
    connect(m_redraw_timer, &QTimer::timeout, this, &GLContextWidget::paintGL);
    m_redraw_timer->start();
}

void GLContextWidget::resizeGL(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0,0, (GLint)w, (GLint)h);
    m_window_size_x = w;
    m_window_size_y = h;
}

void GLContextWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)m_window_size_x / m_window_size_y, 0.1, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0, 0.0,-6.0);

    glm::mat4 rotation_matrix_x(1.0f, 0.0f, 0.0f, 0.0f,
                                0.0f, cosf(m_rot_x), sinf(m_rot_x), 0.0f,
                                0.0f, -sinf(m_rot_x), cosf(m_rot_x), 0.0f,
                                0.0f,0.0f,0.0f,1.0f);

    glm::mat4 rotation_matrix_y(cosf(m_rot_y), 0.0f, -sinf(m_rot_y), 0.0f,
                                0.0f, 1.0f, 0.0f, 0.0f,
                                sinf(m_rot_y), 0.0f, cosf(m_rot_y), 0.0f,
                                0.0f,0.0f,0.0f,1.0f);

    glm::mat4 rotation_matrix_z(cosf(m_rot_z), -sinf(m_rot_z), 0.0f, 0.0f,
                                sinf(m_rot_z), cosf(m_rot_z), 0.0f, 0.0f,
                                0.0f, 0.0f, 1.0f, 0.0f,
                                0.0f, 0.0f, 0.0f, 1.0f);
    std::vector<glm::vec4> vertices {
        glm::vec4(1.0, -1.0, -1.0, 1.0),
                glm::vec4(1.0, -1.0, 1.0, 1.0),
                glm::vec4(0.0, 1.0, 0.0,1.0),

                glm::vec4( 1.0,-1.0, 1.0, 1.0),
                glm::vec4(-1.0,-1.0, 1.0, 1.0),
                glm::vec4(0.0,1.0,0.0, 1.0),

                glm::vec4(-1.0,-1.0,1.0, 1.0),
                glm::vec4(-1.0, -1.0,-1.0, 1.0),
                glm::vec4(0.0,1.0,0.0, 1.0),

                glm::vec4(-1.0,-1.0,-1.0, 1.0),
                glm::vec4(1.0,-1.0,-1.0, 1.0),
                glm::vec4(0.0, 1.0,0.0, 1.0),

                glm::vec4(1.0,-1.0, 1.0, 1.0),
                glm::vec4(-1.0,-1.0, 1.0, 1.0),
                glm::vec4(-1.0,-1.0,-1.0, 1.0),
                glm::vec4(1.0,-1.0,-1.0, 1.0)
    };

    for (auto & v : vertices)
    {
        v = rotation_matrix_x * v;
        v = rotation_matrix_y * v;
        v = rotation_matrix_z * v;
    }

    glBegin(GL_TRIANGLES);
    glColor3f(0.52,0.44,1.0);

    for (int i = 0; i < 3; ++i)
        glVertex3f(vertices[i].x, vertices[i].y, vertices[i].z);
    glEnd();

    glBegin(GL_TRIANGLES);
    glColor3f(1.0,0.84,0.0);

    for (int i = 3; i < 6; ++i)
        glVertex3f(vertices[i].x, vertices[i].y, vertices[i].z);
    glEnd();

    glBegin(GL_TRIANGLES);
    glColor3f(0.94,0.5,0.5);

    for (int i = 6; i < 9; ++i)
        glVertex3f(vertices[i].x, vertices[i].y, vertices[i].z);
    glEnd();

    glBegin(GL_TRIANGLES);
    glColor3f(0.0,1.0,0.0);

    for (int i = 9; i < 12; ++i)
        glVertex3f(vertices[i].x, vertices[i].y, vertices[i].z);
    glEnd();

    glBegin(GL_QUADS);
    glColor3f(1.0,0.51,0.28);

    for (int i = 12; i < 16; ++i)
        glVertex3f(vertices[i].x, vertices[i].y, vertices[i].z);
    glEnd();

    this->swapBuffers();
}

void GLContextWidget::setRot_z(float rot_z)
{
    m_rot_z = glm::radians(rot_z);
    paintGL();
}

void GLContextWidget::setRot_y(float rot_y)
{
    m_rot_y = glm::radians(rot_y);
    paintGL();
}

void GLContextWidget::setRot_x(float rot_x)
{
    m_rot_x = glm::radians(rot_x);
    paintGL();
}
